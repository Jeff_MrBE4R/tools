BootStrap: docker
From: debian:10

%labels
    Author IGBMC

%files
    /src/ccp4-ccpem-modeller/7.1.004-1.5.0-9.25/ccp4-7.1.004-shelx-arpwarp-linux64.tar.gz /opt

%post
    # install system dependencies
    apt-get -y -qqqq update
    apt-get -y install bsdtar\
    csh\
    curl\
    ffmpeg\
    grace\
    imagemagick\
    libcgi-pm-perl\
    libfontconfig1-dev\
    libgl1-mesa-dev\
    libglu1-mesa-dev\
    libncurses5\
    libsm6\
    libxcursor1\
    libxcomposite1\
    libxdamage1\
    libxext-dev\
    libxmu6\
    libxrandr2\
    libxrender-dev\
    libxt6\
    libxxf86vm1\
    mesa-utils\
    openbabel\
    openjdk-11-jre\
    php-cli\
    povray\
    pymol\
    tcsh\
    texlive-latex-extra\
    wget

    # build and install ccp4
    cd /opt
    bsdtar xvzf ccp4-7.1.004-shelx-arpwarp-linux64.tar.gz
    touch $HOME/.agree2ccp4v6
    cd /opt/ccp4-7.1
    ./BINARY.setup
    rm /opt/ccp4-7.1.004-shelx-arpwarp-linux64.tar.gz

    # load ccp4 environment
    . /opt/ccp4-7.1/bin/ccp4.setup-sh
    . /opt/arp_warp_8.0/arpwarp_setup.bash

    # install xds dependencies
    apt-get -y install\
    build-essential\
    python-pip\
    zlib1g-dev\
    cmake #for neggia

    # install xds
    cd /opt
    wget ftp://ftp.mpimf-heidelberg.mpg.de/pub/kabsch/XDS-INTEL64_Linux_x86_64.tar.gz
    tar xvzf XDS-INTEL64_Linux_x86_64.tar.gz
    export PATH=/opt/XDS-INTEL64_Linux_x86_64:$PATH
    rm /opt/XDS-INTEL64_Linux_x86_64.tar.gz

    # install XDS-Viewer,  XDSSTAT, XDSCC12, XDSGUI, XSCALE_ISOCLUSTER
    # dependencies first
    apt-get -y install\
    libqt5test5 libqt5opengl5 libqt5printsupport5\
    xxdiff

    mkdir -p /opt/xds_gui && cd /opt/xds_gui
    wget -N -r -nd ftp://strucbio.biologie.uni-konstanz.de/pub/linux_bin/
    chmod a+x *
    ln -s xdscc12 xscalecc12
    ln -s XDS-viewer xdsviewer
    ln -s XDS-viewer xds-viewer
    export PATH=/opt/xds_gui:$PATH

    cd /tmp
    wget https://github.com/dectris/neggia/tarball/master
    tar xzvf master
    cd dectris-neggia-*
    mkdir build
    cd build
    cmake ..
    make
    mkdir /opt/neggia
    mv src/dectris/neggia/libneggia_static.a /opt/neggia
    mv src/dectris/neggia/plugin/dectris-neggia.so /opt/neggia
    cd ../..
    rm -rf dectris-neggia-*

    #fix for weird alias-related error messages 'alias: could not parse "pushd $CCP4>/dev/null": 1:12: > is not a valid word'
    head -n -13 /opt/ccp4-7.1/bin/ccp4.setup-sh >/ccp4.setup-sh
    mv /ccp4.setup-sh /opt/ccp4-7.1/bin/ccp4.setup-sh

    # cleanup
    apt-get remove -y bsdtar wget
    rm -rf /var/lib/apt/lists/*



%environment
    export LC_ALL=C
    export LD_LIBRARY_PATH=/usr/lib/x86_64-linux-gnu/

    . /opt/ccp4-7.1/bin/ccp4.setup-sh
    . /opt/arp_warp_8.0/arpwarp_setup.bash

    export PATH=/opt/XDS-INTEL64_Linux_x86_64:$PATH
    export PATH=/opt/xds_gui:$PATH
